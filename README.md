# Get started quickly with wagtail
## This repository contains a custom whitelabel wagtail installation
### To quickly hit it of with your future projects
#### yeah.

## including
* npm vendors bootstrap, fontawesome, jquery (install with ```npm i``` in  `mysite/static/vendor`)
* sass preprocessor with example file `mysite/static/mysite.scss` (loads bootstrap)
* wagtail menus
* richtext editor with text-aligning (center, left, right, justify)
* external links in new tab
* custom flexpage model with streamfield using both raw html and richtext
* bootstrap based gridding 3-part using the custom flexpage model

## prepare for prod
* edit production.py (secret key and allowed hosts)
* run with ```gunicorn --workers 3 -b 0.0.0.0:80 mysite.wsgi```
