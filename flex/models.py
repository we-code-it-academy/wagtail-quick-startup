from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.fields import StreamField

# Create your models here.
from streams import blocks

from wagtail.core.models import Page

class FlexPage(Page):
    """Flexible page class."""

    template = "flex_page.html"

    content = StreamField(
        [
            ("one_column", blocks.OneColumnBlock()),
            ("two_columns", blocks.TwoColumnsBlock()),
            ("three_columns", blocks.ThreeColumnsBlock()),
            ("one_third_two_thirds", blocks.OneThirdTwoThirdsColumnBlock()),
            ("two_thirds_one_third", blocks.TwoThirdsOneThirdColumnBlock())
        ],
        null=True,
        blank=True,
    )

    content_panels = Page.content_panels + [
        StreamFieldPanel("content"),
    ]
