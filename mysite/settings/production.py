from .base import *
import os

DEBUG = False

SECRET_KEY = 'samplekey'

ALLOWED_HOSTS = ['127.0.0.1']

INSTALLED_APPS = INSTALLED_APPS + [
    # ...
    'django_extensions',
]

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': 'WARNING',
    },
}

try:
    from .local import *
except ImportError:
    pass
