Django>=3.1,<3.2
wagtail>=2.11,<2.12
django-extensions==3.1.0
django-libsass==0.8
django-sass-processor==0.8.2
gunicorn==20.0.4
wagtailmenus==3.0.2