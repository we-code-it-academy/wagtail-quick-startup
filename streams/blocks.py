from wagtail.core import blocks
from wagtail.core.fields import StreamField
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.blocks import ImageChooserBlock


class ContentBlock(blocks.StreamBlock):
    richtext = blocks.RichTextBlock(blank=True)
    raw = blocks.RawHTMLBlock()

    class Meta:
        template = 'content_block.html'
        icon = 'placeholder'


class StreamBlockBase(blocks.StructBlock):
    section_classes = blocks.CharBlock(classname="section classes", required=False)
    container_classes = blocks.CharBlock(classname="container classes", required=False)


class TwoColumnsBlock(StreamBlockBase):
    left_column = ContentBlock()
    right_column = ContentBlock()

    class Meta:
        template = 'two_columns_block.html'
        icon = 'placeholder'
        label = 'Two Columns'


class OneColumnBlock(StreamBlockBase):
    content = ContentBlock()

    class Meta:
        template = 'one_column_block.html'
        icon = 'placeholder'
        label = 'One Column'


class ThreeColumnsBlock(StreamBlockBase):
    left_column = ContentBlock()
    center_column = ContentBlock()
    right_column = ContentBlock()

    class Meta:
        template = 'three_columns_block.html'
        icon = 'placeholder'
        label = 'Three Columns'


class OneThirdTwoThirdsColumnBlock(StreamBlockBase):
    left_column = ContentBlock()
    right_column = ContentBlock()

    class Meta:
        template = 'one_third_two_thirds_block.html'
        icon = 'placeholder'
        label = 'One Third Two Thirds'


class TwoThirdsOneThirdColumnBlock(StreamBlockBase):
    left_column = ContentBlock()
    right_column = ContentBlock()

    class Meta:
        template = 'two_thirds_one_third_block.html'
        icon = 'placeholder'
        label = 'Two Thirds One Third'
